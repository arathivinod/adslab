#include<stdio.h>
#include<stdlib.h>

struct node
{
    int color;
    int key;
    struct node *left;
    struct node *right;
    struct node *parent;
};

struct node *root = NULL;

struct node *insert(struct node *root, struct node *temp)
{
    if(root == NULL)
  	return temp;
    if(root->key < temp->key){
        root->right = insert(root->right,temp);
        root->right->parent=root;
     }
    else if(root->key > temp->key){
        root->left = insert(root->left,temp);
        root->left->parent=root;
    }
    return root;
}

void rbt(struct node* root, struct node* pt)
{
    struct node* parent_pt = NULL;
    struct node* grand_parent_pt = NULL;
 
    while ((pt != root) &&(pt->color != 0)&& (pt->parent->color == 1))
    {
        parent_pt = pt->parent;
        grand_parent_pt = pt->parent->parent;
 
        /*  Case : A
             Parent of pt is left child
             of Grand-parent of
           pt */
        if (parent_pt == grand_parent_pt->left)
        {
 
            struct node* uncle_pt = grand_parent_pt->right;
 
            /* Case : 1
                The uncle of pt is also red
                Only Recoloring required */
            if (uncle_pt != NULL && uncle_pt->color == 1)
            {
                grand_parent_pt->color = 1;
                parent_pt->color = 0;
                uncle_pt->color = 0;
                pt = grand_parent_pt;
            }
 
            else {
 
                /* Case : 2
                     pt is right child of its parent
                     Left-rotation required */
                if (pt == parent_pt->right) {
                    leftrotate(parent_pt);
                    pt = parent_pt;
                    parent_pt = pt->p;
                }
 
                /* Case : 3
                     pt is left child of its parent
                     Right-rotation required */
                rightrotate(grand_parent_pt);
                int t = parent_pt->c;
                parent_pt->c = grand_parent_pt->c;
                grand_parent_pt->c = t;
                pt = parent_pt;
            }
        }
 
        /* Case : B
             Parent of pt is right
             child of Grand-parent of
           pt */
        else {
            struct node* uncle_pt = grand_parent_pt->l;
 
            /*  Case : 1
                The uncle of pt is also red
                Only Recoloring required */
            if ((uncle_pt != NULL) && (uncle_pt->c == 1))
            {
                grand_parent_pt->c = 1;
                parent_pt->c = 0;
                uncle_pt->c = 0;
                pt = grand_parent_pt;
            }
            else {
                /* Case : 2
                   pt is left child of its parent
                   Right-rotation required */
                if (pt == parent_pt->l) {
                    rightrotate(parent_pt);
                    pt = parent_pt;
                    parent_pt = pt->p;
                }
 
                /* Case : 3
                     pt is right child of its parent
                     Left-rotation required */
                leftrotate(grand_parent_pt);
                int t = parent_pt->c;
                parent_pt->c = grand_parent_pt->c;
                grand_parent_pt->c = t;
                pt = parent_pt;
            }
        }
    }
 
    root->c = 0;
}
void inorder(struct node *root)
{
    if(root == NULL){
        return;
      }
    inorder(root->left);
    printf("%d ",root->key);
    inorder(root->right);
}


int main()
{
int item,c;
while(1)
{
printf("\n1.insert\n2.delete\n3.inordertraversal\n4.search\n5.display\n6.exit\n");
printf("enter the case number:");
scanf("%d",&c);
switch(c)
{
case 1:printf("\n Enter the data to be inserted: ");
    	scanf("%d",&item);
    	struct node *newNode = malloc(sizeof(struct node));
    	newNode->key   = item;
    	newNode->left  = NULL;
    	newNode->right = NULL;
    	newNode->parent = NULL;
    	newNode->color = 1;
    	
    	root=insert(root,newNode);
    	rbt(root,temp);
    	break;
case 2: printf("\n Enter the data to be deleted: ");
   	 scanf("%d",&item);
    	root = removeNode(root,item);
	break;
case 3:inorder(root);
	break;
case 4:printf("\n enter the Key to be searched: ");
	scanf("%d",&item);
	search(root,item);
	break;
case 5:exit(0);
	break;
}
}
  
    printf("\n");

    return 0;
}
